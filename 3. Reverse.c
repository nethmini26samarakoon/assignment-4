#include <stdio.h>
#include <stdlib.h>

int main()
{
   int n, m = 0, remainder;
    printf("Enter an integer: ");
    scanf("%d", &n);
    while (n != 0)
        {
        remainder = n % 10;
        m = m * 10 + remainder;
        n /= 10;
    }
    printf("Reversed number = %d", m);
    return 0;
}
