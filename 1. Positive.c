#include <stdio.h>
#include <stdlib.h>

int main()
{
    int total=0, N=0;
    do {printf("Input number: ");
    scanf("%d",&N);
    if(N<=0) break;
    total+=N;}
    while (N>0);
    printf("Total %d \n", total);
    return 0;
}
