#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num,k;
    printf("Input a positive integer: ");
    scanf("%d", &num);
    printf("Factors of %d are:",num);
    k=1;
    while(k<=num)
        {if(num%k==0)
                     printf("%d,",k);
            k++;}
    printf("\n");
    return 0;
}
